package klvl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class SeleniumTest {

    public final String SELENOID_URL = "http://host.docker.internal:4444/wd/hub";
    public WebDriver driver;

    @BeforeMethod
    public void preCondition() {
        driver = initDriver();
    }

    private WebDriver initDriver() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("browserName", "chrome");
        capabilities.setCapability("browserVersion", "100.0");
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", true);
        try {
            return  new RemoteWebDriver(new URL(SELENOID_URL), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Test
    public void simpleTest() {
        driver.get("https://google.com/");
        String sessionId = String.valueOf(((RemoteWebDriver) driver).getSessionId());
        System.out.println("Session ID: " + sessionId);
    }
}
